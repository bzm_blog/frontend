import { createRouter, createWebHistory } from 'vue-router'
import ArticlesPage from '@/views/ArticlesPage.vue'
import MainPage from '@/views/MainPage.vue'
import ArticleItemPage from '@/views/ArticleItemPage.vue'
import AddArticlePage from '@/views/AddArticlePage.vue'
import Login from '@/views/Login.vue'
import SignUp from '@/views/SignUp.vue'
import CollbackPage from '@/views/CollbackPage.vue'
import store from '@/store/index'
import Logout from '@/views/Logout.vue'

const routes = [
  {
    path: '/',
    name: 'main',
    component: MainPage
  },
  {
    path: '/articles',
    name: 'articles',
    component: ArticlesPage,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/article/:id',
    name: 'articleItem',
    component: ArticleItemPage,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/AddArticlePage',
    name: 'addArticle',
    component: AddArticlePage,
    meta: { 
      requiresAuth: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  },
  {
    path: '/signup',
    name: 'signup',
    component: SignUp
  },
  {
    path: '/callback',
    name: 'callback',
    component: CollbackPage
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.auth.isAuth) {
      next()
      return
    }
    next('/login') 
  } else {
    next() 
  }
})

export default router;
