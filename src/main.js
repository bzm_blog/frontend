import { createApp } from 'vue';
import App from './App.vue';
import router from '@/router/router';
import store from '@/store/index';
import components from '@/components/UI/';
import axios from 'axios';
const app = createApp(App);

axios.defaults.baseURL = 'http://127.0.0.1:8000'

components.forEach(component => {
    app.component(component.name, component)
})


app
    .use(router, axios)
    .use(store)
    .mount('#app')
