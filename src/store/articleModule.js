import axios from 'axios';
export const articleModule = {
    state: () => ({
        articles: [],
        isLoading: false,
        page: 1,
        limit: 3,
        totalPages: 0,
        articleItem: [],
        articleItemId: 0,
    }),
    getters: {
    },
    mutations: {
        setArticles(state, articles){
            state.articles = articles;
        },
        setArticleItem(state, article){
            state.articleItem = article;
        },
        setArticleItemId(state, id){
          state.articleItemId = id;
        },
        setIsLoading(state, bool){
            state.isLoading = bool;
        },
        setPage(state, pageNumber){
            state.page = pageNumber;
        },
        setTotalPages(state, totalPages){
            state.totalPages = totalPages;
        },
    },
    actions: {
        async fetchArticles({state, commit}) {
            commit('setIsLoading', true);
            const host = process.env.VUE_APP_API_HOST;
            const port = process.env.VUE_APP_API_PORT;
            try{
              const response = await axios.get(`http://${host}:${port}/articles/?format=json`, {
                params: {
                  offset: ((state.page-1)*state.limit),
                }
              });
              commit('setTotalPages',  Math.ceil(response.data.count / state.limit));
              commit('setArticles', response.data.results);
            }catch (e){
                console.log(e);
                alert(e);
            }finally {
                commit('setIsLoading', false);
            }
          },
          async fetchArticleItem({state, commit}, id) {
            const host = process.env.VUE_APP_API_HOST;
            const port = process.env.VUE_APP_API_PORT;
            try{  
              const response = await axios.get(`http://${host}:${port}/articles/${id}/?format=json`);
              commit('setArticleItem', response.data);
            }catch (e){
                alert(e);
            }
          },
          async postArticle({state, commit}, articleObject){
            const host = process.env.VUE_APP_API_HOST;
            const port = process.env.VUE_APP_API_PORT;
            try{  
              await axios
                .post(`http://${host}:${port}/articles/create/`, articleObject)
            }catch (e){
                alert(e);
            }
          }
    },
    namespaced: true
}