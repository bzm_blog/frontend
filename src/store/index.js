import { createStore } from 'vuex'
import {articleModule} from '@/store/articleModule'
import {authModule} from '@/store/authModule'

export default createStore({
  modules: {
    article: articleModule, 
    auth: authModule,
  },
})
