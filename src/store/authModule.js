import firebaseConfig from '@/firebaseConfig';
import axios from 'axios';
import { getAuth, signInWithPopup, signOut, GoogleAuthProvider, GithubAuthProvider } from "firebase/auth";

firebaseConfig

const providerGoogle = new GoogleAuthProvider();
const providerGithub = new GithubAuthProvider();
const auth = getAuth();

export const authModule = {
    
    namespaced: true,
    state:{
        token: '', 
        isAuth: false,
        user: '',
        firebase_username: 'firebase',
        firebase_password: 'f3ire1bas4e_a2uth',
    },
    mutations: {
        initializeStore(state){
            if(localStorage.getItem('auth/token')){
                state.token = localStorage.getItem('auth/token');
                state.isAuth = true;
            }else{
                state.token = '';
                state.isAuth = false;
            }
        },
        setToken(state, token){
            state.token = token;
            state.isAuth=true;
        },
        removeToken(state){
            state.token='';
            state.isAuth=false;
            localStorage.setItem('auth/token', '');
        }
    },

    getters: {
    },
    actions: {
        loginToBe({state, commit}, formData){
            axios
                .post('token_auth/token/login', formData)
                .then(response => {
                    console.log(response)
                    const token = response.data.auth_token;
    
                    commit('auth/setToken', token);
    
                    axios.defaults.headers.common['Authorization'] = 'Token ' + token;
                    
                    state.token = token;

                    $router.push('/articles'); 
                })
                .catch(e => {
                    console.log(e.response.data)
                })
            },
            firebaseloginToBe({state, commit}){
                const formData = {
                    username: state.firebase_username,
                    password: state.firebase_password
                };
                axios
                    .post('token_auth/token/login', formData)
                    .then(response => {
                        console.log(response)
                        const token = response.data.auth_token;
        
                        commit('setToken', token);
        
                        axios.defaults.headers.common['Authorization'] = 'Token ' + token;
                        localStorage.setItem('auth/token', token);
                        $router.push('/articles'); 
                    })
                    .catch(e => {
                        console.log(e.response)
                    })
                },
        oauthLoginGoogle({state, commit}){
            try{
                signInWithPopup(auth, providerGoogle)
                    .then((result) => {          
                    state.user = result.user.email;
                    this.dispatch('auth/firebaseloginToBe');
                    console.log(result);
            });
            
        }catch(e){
            console.log('HELLO FROM catch');
            console.log(e);
        }
        },
        oauthLoginGitHub({state, commit}){
            try{
                signInWithPopup(auth, providerGithub)
                    .then((result) => {          
                    state.user = result.user.email;
                    this.dispatch('auth/firebaseloginToBe');
                });
                
            }catch(e){
                console.log('HELLO FROM catch');
                console.log(e);
            }
        }
    },
   
}