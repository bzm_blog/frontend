import { initializeApp } from "firebase/app";

const firebaseConfig = {

    apiKey: "AIzaSyCv8zXxDv5DNsKERkn-WvtyHjjvzCQMbTo",
  
    authDomain: "blog-372119.firebaseapp.com",
  
    projectId: "blog-372119",
  
    storageBucket: "blog-372119.appspot.com",
  
    messagingSenderId: "520623491517",
  
    appId: "1:520623491517:web:4c074890c247f4eb06aad4"
  
  };

const firebaseApp = initializeApp(firebaseConfig);

export default firebaseApp