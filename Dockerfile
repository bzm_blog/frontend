FROM node
WORKDIR /src
COPY package*.json ./
COPY . /src/
RUN npm install
EXPOSE 8080
RUN cd src/
CMD ["npm", "run", "serve"]